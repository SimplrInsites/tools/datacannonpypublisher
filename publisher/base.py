"""
Extensible base publisher for other classes to use. This is where weird
send the message. We use the strategy pattern to decide how to publish.
"""


class BasicPublisher(object):

    def __init__(self, adapter):
        """
        Constructor

        :param adapter: The adapter to plug into the system
        """
        pass

    def publish(self):
        """
        Publishes the message
        :return:
        """
        pass
