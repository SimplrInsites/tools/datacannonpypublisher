"""
Template for implementing adapters. Standardizes inputs for the constructor and
function names. Please extend this.
"""


class BaseSinkAdapter(object):

    def __init__(self, params):
        """
        Constructor
        """
        self._params = params

    def close(self):
        """
        Overwrite this and close your connections in this method. Use atexit to ensure this
        happens
        """
        raise RuntimeError("Unimplemented")

    def publish(self, msg):
        """
        Publishes the message to the queue.

        :param msg: The message to publish
        """
        raise RuntimeError("Unimplemented")
