"""
Creates a RabbitMQ Publisher to send connections over
"""
from publisher.adapter.base import BaseSinkAdapter


class RabbitMQ(BaseSinkAdapter):

    def __init__(self, params):
        """
        Constructor
        """
        super().__init__(params)

    def close(self):
        """
        Closes the channel and connections.
        """
        raise RuntimeError("Unimplemented")

    def publish(self, msg):
        """
        Publishes the message to the queue.

        :param msg: The message to publish
        """
        raise RuntimeError("Unimplemented")
