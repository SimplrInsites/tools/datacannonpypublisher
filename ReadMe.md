# Data Cannon Publisher in Python

This tool is pure or at least mostly pure Python. It allows
you to take advantage of the Rust based Data Cannon System
for asynchronous task processing. Pass information to the publisher.

# Reasoning

When you want to stream hundreds of sources of data or process
many tasks in micro-batching or a more traditional batch strategy,
tools in Python become highly convoluted. For example, Pika chooses 
an event loop, exposing a poor interface to whichever you choose.

Still, ETL and ingestion is over-burdensome in most other languages.
Who needs 500 lines of boilerplate code when 50 lines will do.

Data Cannon was meant to replace this issue and make it easy to run code
on both Python and Rust.  We want to eliminate complexity, make streaming
closed yet extensible, and get rid of quality issues in data and software
without the buying systems that cost an entire salary of a US developer every
year. 

It is still baffling as to why I have to spend $3k - $10k per developer
per month to use an ETL tool that can handle long network requests, sip memory
in NLP, and tackle modern issues correctly. 

Power to the people. Why should large corporations be stuck creating scalable data products? 

I suspect this project will morph as Python and Rust interact a little.

Cheers!

# Target Features:

Target features are to:

    - Block or asynchronously process returned values.
    - Standardize input into the Data Cannon System 
    - Offer non-blocking task generation for other elements in the system
    - Interact with RabbitMQ, Kafka, and any other target sink

This is a sink tool that will hopefully include some Rust in it.

# Modules

The standard interface is in the publisher module. Utilities are mostly
for internal use.

# License

See the license. Thanks